﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace Exercice04
{
    class Program
    {
        static void Main(string[] args)
        {
            try{
                String jsonString = File.ReadAllText(".\\resources\\produits.json");
                Console.WriteLine(jsonString);

                List<Product> liste = JsonSerializer.Deserialize<List<Product>>(jsonString);
                for (int i=0; i<liste.Count; i++){
                    Console.WriteLine(liste[i].toString());
                }

            } catch (Exception ex) {
                Console.WriteLine(ex.Message);

            } 

            Console.ReadKey();
        }
    }
}
