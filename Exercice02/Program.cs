﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Exercice02
{
    class Program
    {
        static void Main(string[] args)
        {
            try{
                StreamWriter sw = new StreamWriter(".\\resources\\unProduit.json");
                
                List<Product> liste = new List<Product>();
                liste.Add(new Product ("Iphone", "Vert Foret", 1950));
                liste.Add(new Product ("Laptop Dell", "Noir", 2499));
                liste.Add(new Product ("Souris", "Gris", 199));    

                string jsonString;
                jsonString = JsonSerializer.Serialize(liste);
                sw.Write(jsonString);
                sw.Close();

                Console.Write("Ecriture dans le fichier terminer - le dossier est dans le dossier ressources");
            
            } catch (Exception ex) {
                Console.WriteLine (ex.Message);
            } 
            Console.ReadKey();
        }
    }
}
