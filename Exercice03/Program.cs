﻿using System;
using System.Text.Json;
using System.IO;

namespace Exercice03
{
    class Program
    {
        static void Main(string[] args)
        {
           try {
               String jsonString = File.ReadAllText(".\\resources\\produits.json");
               Console.WriteLine(jsonString);
               Product produit = JsonSerializer.Deserialize<Product>(jsonString);
               Console.WriteLine(produit.toString());
           } catch (Exception ex) {
               Console.WriteLine(ex.Message);
           } 

           Console.ReadKey();

        }
    }
}
