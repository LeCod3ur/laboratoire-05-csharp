using System;
using System.Collections.Generic;
using System.Text;

namespace Lab05
{
    public class Product
    {
        public string nom {get; set;}
        public string couleur {get; set;}

        public double prix {get; set;}

        public Product() {

        }

        public Product(string nom, string couleur, double prix) {
            this.nom = nom;
            this.couleur = couleur;
            this.prix = prix;
        }

        public string toString(){
            return nom + " " + couleur + " " + prix;
        }
    }
}