﻿using System;
using System.IO;
using System.Text.Json;

namespace Lab05
{
    class Program
    {
        static void Main(string[] args)
        {
            try{
                StreamWriter sw = new StreamWriter(".\\resources\\unProduit.json");
                Product pr = new Product("Produit1", "Rouge", 38);
                
                string jsonString;
                jsonString = JsonSerializer.Serialize(pr);
                sw.Write(jsonString);
                sw.Close();

                Console.Write("Ecriture dans le fichier terminer");
            
            } catch (Exception ex) {
                Console.WriteLine (ex.Message);
            } 
            Console.ReadKey();
        }
    }
}
